import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Button,
	Container,
	Row,
	Col,
	Input,
	InputGroup,
	InputGroupAddon,
	InputGroupText
} from 'reactstrap';
import { FaLock, FaUserShield } from 'react-icons/fa';
import Swal from 'sweetalert2';
import { get, post } from '../service/service';

export default class ModalLoginModalLogin extends Component {
	constructor(props) {
		super(props);

		this.state = {
			modal: false
		};
		this.toggle = this.toggle.bind(this);
	}
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	}
	onChangs = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	login = async () => {
		let { password, username } = this.state;
		let obj = {
			password,
			username
		};

		this.props.login(obj);
	};
	KeyEnter = (e) => {
		let { password, username } = this.state;
		let obj = {
			password,
			username
		};
		this.props.enter(obj, e);
	};
	render() {
		let { authen, modal, toggle } = this.props;
		if (!authen) {
			return (
				<div>
					<text onClick={this.toggle}>เข้าสู่ระบบ</text>
					<Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
						<ModalHeader id="style-modal-head" toggle={this.toggle}>
							เข้าสู่ระบบ
						</ModalHeader>
						<ModalBody>
							<Container style={{ paddingTop: '1rem' }}>
								<Row>
									<Col xs={12}>
										<div>
											<div style={{ width: '300px' }}>
												<InputGroup>
													<InputGroupAddon addonType="prepend">
														<InputGroupText>
															<FaUserShield />
														</InputGroupText>
													</InputGroupAddon>
													<Input
														name="username"
														type="text"
														placeholder="username"
														onChange={this.onChangs}
														onKeyDown={this.KeyEnter}
													/>
												</InputGroup>
											</div>
											<br />
											<div style={{ width: '300px' }}>
												<InputGroup>
													<InputGroupAddon addonType="prepend">
														<InputGroupText>
															<FaLock />
														</InputGroupText>
													</InputGroupAddon>
													<Input
														name="password"
														type="password"
														placeholder="password"
														onChange={this.onChangs}
														onKeyDown={this.KeyEnter}
													/>
												</InputGroup>
											</div>
										</div>
									</Col>
									<Col xs={12} />
								</Row>
							</Container>
						</ModalBody>
						<ModalFooter>
							<Button onClick={this.login} color="primary">
								ตกลง
							</Button>
							<Button onClick={this.toggle} color="danger">
								ยกเลิก
							</Button>
						</ModalFooter>
					</Modal>
				</div>
			);
		} else {
			return (
				<Modal isOpen={modal} toggle={toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={toggle}>
						เข้าสู่ระบบ
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<div>
										<div style={{ width: '300px' }}>
											<InputGroup>
												<InputGroupAddon addonType="prepend">
													<InputGroupText>
														<FaUserShield />
													</InputGroupText>
												</InputGroupAddon>
												<Input
													name="username"
													type="text"
													placeholder="username"
													onChange={this.onChangs}
												/>
											</InputGroup>
										</div>
										<br />
										<div style={{ width: '300px' }}>
											<InputGroup>
												<InputGroupAddon addonType="prepend">
													<InputGroupText>
														<FaLock />
													</InputGroupText>
												</InputGroupAddon>
												<Input
													name="password"
													type="password"
													placeholder="password"
													onChange={this.onChangs}
												/>
											</InputGroup>
										</div>
									</div>
								</Col>
								<Col xs={12} />
							</Row>
						</Container>
					</ModalBody>
					<ModalFooter>
						<Button onClick={this.login} color="primary">
							ตกลง
						</Button>
						<Button onClick={toggle} color="danger">
							ยกเลิก
						</Button>
					</ModalFooter>
				</Modal>
			);
		}
	}
}
