import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from '../pdfmake/build/vfs_fonts';
import moment from 'moment';
import file64 from '../img/filebase64';

let th = require('moment/locale/th');
moment.updateLocale('th', th);
pdfMake.vfs = pdfFonts.pdfMake.vfs;

//ต้องระบุตามชื่อของ ไฟล์ font
pdfMake.fonts = {
	THSarabunNew: {
		normal: 'THSarabunNew.ttf',
		bold: 'THSarabunNew-Bold.ttf',
		italics: 'THSarabunNew-Italic.ttf',
		bolditalics: 'THSarabunNew-BoldItalic.ttf'
	},
	Roboto: {
		normal: 'Roboto-Regular.ttf',
		bold: 'Roboto-Medium.ttf',
		italics: 'Roboto-Italic.ttf',
		bolditalics: 'Roboto-MediumItalic.ttf'
	}
};
export const slip = (data, detail) => {
	let name = data[0].fname + ' ' + data[0].lname;
	let a = Object.entries(detail[0]);
	var docDefinition = {
		footer: function(currentPage, pageCount) {
			return [
				{
					text: currentPage.toString() + ' of ' + pageCount,
					alignment: 'right',
					margin: [ 0, 0, 40, 0 ]
				}
			];
		},
		content: [
			{
				margin: [ 0, 0, 0, 0 ],
				text: 'ใบเสร็จ' + data[0].type_payment,
				alignment: 'center',
				fontSize: 25,
				bold: true
			},
			{
				margin: [ 0, 0, 0, 15 ],
				alignment: 'justify',
				columns: [
					[
						{
							width: '50%',
							columns: [
								{
									width: 70,
									height: 70,
									image: file64.img_head
								}
							]
						}
					],
					[
						{
							text: 'เลขที่เอกสารรายการจอง: ' + data[0].id_booking, //ใส่ค่าที่ต้องการ
							alignment: 'right',
							fontSize: 15
						},
						{
							text: 'วันที่ชำระ : ' + moment(data[0].timestam).add(543, 'y').format('LL'), //ใส่ค่าที่ต้องการ
							alignment: 'right',
							fontSize: 15
						},
						{
							text: 'ชื่อผู้จอง : ' + name, //ใส่ค่าที่ต้องการ
							alignment: 'right',
							fontSize: 15
						}
					]
				]
			},
			{
				margin: [ 0, 0, 0, 0 ],
				table: {
					widths: [ '20%', '80%' ],
					heights: [ 15, 15 ],
					body: [
						[
							{
								margin: [ 5, 0, 0, 0 ],
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'รายการ',
								fontSize: 17
							},
							{
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'รายละเอียด',
								fontSize: 17
							}
						]
					]
				}
			},
			a.map((el, i) => {
				return {
					columns: [
						{
							margin: [ 10, 0, 0, 0 ],
							width: '20%',
							text: el[0],
							fontSize: 16
						},
						{
							margin: [ 10, 0, 0, 0 ],
							width: '80%',
							text: el[1],
							fontSize: 16
						}
					]
				};
			}),
			{
				margin: [ 0, 0, 0, 10 ],
				table: {
					widths: [ '50%', '50%' ],
					heights: [ 15, 15, 15, 15 ],
					body: [
						[
							{
								// width: ,
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'ค่าบริการ',
								fontSize: 17
							},
							{
								// width: ,
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text:
									data[0].type_payment === 'ชำระค่ามัดจำ'
										? data[0].deposit + ' บาท'
										: data[0].amount_meeting + ' บาท',
								fontSize: 17,
								alignment: 'right'
							}
						],
						[
							{
								// width: ,
								border: [ false, true, false, true ],
								fillColor: '#e6f7ff',
								text: 'รวมมูลค่าทั้งหมด',
								fontSize: 17
							},
							{
								// width: ,
								border: [ false, true, false, true ],
								fillColor: '#e6f7ff',
								text:
									data[0].type_payment === 'ชำระค่ามัดจำ'
										? data[0].deposit + ' บาท'
										: data[0].amount_meeting + ' บาท',
								fontSize: 17,
								alignment: 'right'
							}
						]
					]
				}
			},
			{
				text: 'ลงชื่อ สุนันท์ พิสิสิทธิกุล ผู้ดูแล',
				fontSize: 17,
				alignment: 'right'
			},
			{
				text: 'วันที่ ' + moment(data[0].timestam).add(543, 'y').format('LL'),
				fontSize: 17,
				alignment: 'right'
			}
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
