import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Container,
	Row,
	Col,
	Input,
	Form,
	FormGroup,
	Label,
	Button
} from 'reactstrap';
import moment from 'moment';
import XlsExport from 'xlsexport';
import { FaLock, FaUserShield, FaPrint } from 'react-icons/fa';
import { post, get } from '../service/service';
import Swal from 'sweetalert2';

export default class ModalService extends Component {
	constructor(props) {
		super(props);

		this.state = {
			price_servicerate: 0,
			id_servicerate: 0
		};
	}

	async componentDidMount() {
		try {
			let res = await get('/get_servicerate');
			this.setState({
				price_servicerate: res.result[0].price_servicerate,
				id_servicerate: res.result[0].id_servicerate
			});
		} catch (error) {
			console.log(error);
		}
	}

	onChangeState = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	async servicerate() {
		try {
			let { price_servicerate, id_servicerate } = this.state;
			let obj = {
				price_servicerate,
				id_servicerate
			};
			let res = await post('/servicerate', obj);
			if (res.success) {
				Swal.fire(res.message, 'แก้ไขอัตราค่าบริการสำเร็จ', 'success').then(() => window.location.reload());
			} else {
				Swal.fire('ผิดพลาด', 'แก้ไขอัตราค่าบริการไม่สำเร็จ', 'error');
			}
		} catch (error) {
			console.log(error);
		}
	}
	render() {
		let { modal, toggle, modalDetail, check } = this.props;
		let { price_servicerate } = this.state;
		return (
			<div>
				<Modal isOpen={modal} toggle={toggle}>
					<ModalHeader id="style-modal-head" toggle={toggle}>
						อัตราค่าบริการลงทะเบียน
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<div>
										<Form>
											<FormGroup row>
												<Label sm={4}>อัตราค่าบริการ</Label>
												<Col sm={8}>
													<Input
														name="price_servicerate"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="number"
														value={price_servicerate}
													/>
												</Col>
											</FormGroup>
										</Form>
									</div>
								</Col>
							</Row>
						</Container>
					</ModalBody>
					<ModalFooter>
						<Button onClick={() => this.servicerate()} color="primary">
							ตกลง
						</Button>
						<Button onClick={toggle} color="danger">
							ยกเลิก
						</Button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}
