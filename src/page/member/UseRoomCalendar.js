import React, { Component } from 'react';
import { Container, Row, Col, Input, Button, Table, Form, FormGroup, Label } from 'reactstrap';
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar/dist/fullcalendar.css';
import { post, get } from '../../service/service';
import moment from 'moment';
import Swal from 'sweetalert2';
import ModalDetail from '../../compoment/ModalDetail';
import Navbar_no_login from '../../compoment/navbar_no_login';

export default class UseRoomCalendar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			events: [],
			modal: false,
			modalDetail: {}
		};
	}
	async componentDidMount() {
		let res = await get('/get_booking');
		this.setState({
			events: res.result.map((e) => ({
				id: e.id_booking,
				title:
					e.timestart_booking + ' - ' + e.timeend_booking + ' ' + e.name_meeting + ' ' + e.location_meeting,
				start: moment(e.datestart_booking).format('YYYY-MM-DD'),
				end: moment(e.dateend_booking).add(1, 'd').format('YYYY-MM-DD'),
				color:
					e.status_booking === 'รอตรวจสอบ'
						? '#008ae6'
						: e.status_booking === 'รอการชำระเงิน' ? '#4dd2ff' : '#4ce600',
				textColor: '#fff'
			}))
		});
	}
	clickModal = async (e) => {
		let res = await post('/get_room_detail', { id: e });
		this.setState({ modalDetail: res.result[0] }, () => this.setState({ modal: true }));
	};
	login = async (e) => {
		if (!e.password || !e.username) {
			Swal.fire('คำเตือน', 'กรอกข้อมูลไม่ครบ', 'warning');
		} else {
			try {
				let res = await post('/login', { username: e.username, password: e.password });
				if (res.success) {
					localStorage.setItem('data', JSON.stringify(res.result[0]));
					if (res.result[0].role === 'renter') {
						this.props.history.push('/calendar');
					} else if (res.result[0].role === 'lessor') {
						this.props.history.push('/SaleHomeTwo');
					} else {
						this.props.history.push('/AdminRate');
					}
				} else {
					Swal.fire('ผิดพลาด', res.message, 'error');
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', error, 'error');
				console.log(error);
			}
		}
	};
	loginEnter = async (e, k) => {
		if (k.key === 'Enter') {
			if (!e.password || !e.username) {
				alert('กรอกข้อมูลไม่ครบ');
			} else {
				try {
					let res = await post('/login', { username: e.username, password: e.password });
					if (res.success) {
						localStorage.setItem('data', JSON.stringify(res.result[0]));
						if (res.result[0].role === 'renter') {
							this.props.history.push('/calendar');
						} else if (res.result[0].role === 'lessor') {
							this.props.history.push('/SaleHomeTwo');
						} else {
							this.props.history.push('/AdminRate');
						}
					} else {
						alert(res.message);
					}
				} catch (error) {
					alert('ผิดพลาด', error);
					console.log(error);
				}
			}
		}
	};
	regis = async (e) => {
		let { role, fname, lname, phone, token_line, username, password, password2 } = e;
		if (!role || !fname || !lname || !phone || !token_line || !username || !password || !password2) {
			Swal.fire('คำเตือน', 'กรอกข้อมูลไม่ครบ', 'warning');
		} else {
			if (password !== password2) {
				Swal.fire('คำเตือน', 'password ไม่ตรงกัน', 'warning');
			} else {
				let user = await get('/user_all');
				let check = user.result.some(
					(e) => String(e.username).toLowerCase() === String(username).toLowerCase()
				);
				if (check) {
					Swal.fire('คำเตือน', 'username นี้ถูกใช้ไปแล้ว', 'warning');
				} else {
					try {
						let res = await post('/insert_user', e);
						if (res.success) {
							Swal.fire('สำเร็จ', 'สมัครสมาชิกเสร็จสิ้น', 'success').then(async () => {
								let ress = await post('/login', { username, password });
								localStorage.setItem('data', JSON.stringify(ress.result[0]));
								if (ress.result[0].role === 'renter') {
									this.props.history.push('/calendar');
								} else if (ress.result[0].role === 'lessor') {
									this.props.history.push('/SaleHomeTwo');
								} else {
									this.props.history.push('/AdminRate');
								}
							});
						} else {
							Swal.fire('ผิดพลาด', 'ไม่สามารถสมัครสมาชิกได้', 'error');
						}
					} catch (error) {
						Swal.fire('ผิดพลาด', error, 'error');
					}
				}
			}
		}
	};
	render() {
		let { events, modal, modalDetail } = this.state;
		return (
			<div>
				<Navbar_no_login
					login={(e) => this.login(e)}
					regis={(e) => this.regis(e)}
					enter={(obj, key) => this.loginEnter(obj, key)}
				/>
				<Container style={{ paddingTop: '1rem' }}>
					<ModalDetail
						modal={modal}
						modalDetail={modalDetail}
						close={() => this.setState({ modal: false })}
					/>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>
										แสดงการใช้งานปฏิทินห้องประชุม
									</div>
								</div>
								<div id="example-component">
									<FullCalendar
										id="your-custom-ID"
										header={{
											left: 'prev,next today myCustomButton',
											center: 'title',
											right: 'month,basicWeek,basicDay'
										}}
										navLinks={true} // can click day/week names to navigate views
										editable={true}
										eventLimit={true} // allow "more" link when too many events
										events={events.length > 0 && events}
										eventClick={(calEvent, jsEvent, view, resourceObj) =>
											this.clickModal(calEvent.id)}
									/>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
