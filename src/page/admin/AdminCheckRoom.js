import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Container,
	Row,
	Col,
	Input,
	Button,
	Table,
	Form,
	FormGroup,
	Label
} from 'reactstrap';
import { get, post, sever } from '../../service/service';
import Swal from 'sweetalert2';
import moment from 'moment';
import { FaLock, FaUserShield, FaFileImage } from 'react-icons/fa';
let api = sever + '/image/room/';
let api_regis = sever + '/image/regis/';

export default class AdminCheckRoom extends Component {
	constructor(props) {
		super(props);

		this.state = {
			list_meeting: [],
			modal: false,
			reg: []
		};
		this.toggle = this.toggle.bind(this);
	}

	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
	}
	async componentDidMount() {
		try {
			let userData = JSON.parse(localStorage.getItem('data'));
			let res = await get('/admin_room');
			let reg = await get('/registration');
			this.setState({
				list_meeting: res.result,
				reg: reg.result
			});
		} catch (error) {
			console.log(error);
		}
	}
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	}
	searchText = async (e) => {
		try {
			let userData = JSON.parse(localStorage.getItem('data'));
			let res = await get('/admin_room');
			let texts = e.toLowerCase();
			let a = res.result
				.map((e) => ({
					...e,
					status_meetings:
						e.status_meeting === 'on'
							? 'ชำระเงินสำเร็จ'
							: e.status_meeting === 'wait' ? 'รอการยืนยัน' : 'รอชำระเงิน'
				}))
				.filter(
					(el) =>
						String(el.name_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.location_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.type_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.status_meetings).toLowerCase().indexOf(texts) > -1
				);
			this.setState({ list_meeting: a });
		} catch (error) {
			console.log(error);
		}
	};
	render() {
		let { list_meeting, modal, id_registration, reg, check } = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>ตรวจสอบรายการจอง</div>
									<div>
										<Input
											type="search"
											placeholder="ค้นหา"
											onChange={(e) => this.searchText(e.target.value)}
											// style={{ float: "right" }}
										/>
									</div>
								</div>
								<div>
									<Table striped>
										<thead>
											<tr>
												<th>รูปภาพ </th>
												<th>ชื่อห้องประชุม</th>
												<th>ประเภท</th>
												<th>สถานที่ตั้ง</th>
												<th>สถานะ</th>
												<th>ราคาลงทะเบียน</th>
												<th />
											</tr>
										</thead>
										<tbody>
											{list_meeting.length > 0 ? (
												list_meeting.map((e) => (
													<tr>
														<td>
															<img
																src={api + e.id_meeting + '.png'}
																width={50}
																height={50}
															/>
														</td>
														<td>{e.name_meeting}</td>
														<td>{e.type_meeting}</td>
														<td>{e.location_meeting}</td>
														<td
															style={{
																color:
																	e.status_meeting === 'on'
																		? 'green'
																		: e.status_meeting === 'wait' ? 'blue' : 'red'
															}}
														>
															{e.status_meeting === 'on' ? (
																'ชำระเงินสำเร็จ'
															) : e.status_meeting === 'wait' ? (
																'รอการยืนยัน'
															) : (
																'รอชำระเงิน'
															)}
														</td>
														<td>{e.servicerate.toLocaleString()} บาท</td>
														<td>
															{e.status_meeting === 'off' ? (
																'ยังไม่ชำระค่าลงทะเบียน'
															) : e.status_meeting === 'wait' ? (
																<Button
																	outline
																	size="sm"
																	color="success"
																	onClick={() =>
																		this.setState(
																			{
																				id_registration: reg.filter(
																					(el) =>
																						el.id_m_regis === e.id_meeting
																				)[0].id_registration,
																				check: true,
																				id_meeting: e.id_meeting
																			},
																			() => this.toggle()
																		)}
																>
																	<FaFileImage />
																</Button>
															) : (
																<Button
																	outline
																	size="sm"
																	color="success"
																	onClick={() =>
																		this.setState(
																			{
																				id_registration: reg.filter(
																					(el) =>
																						el.id_m_regis === e.id_meeting
																				)[0].id_registration,
																				check: false
																			},
																			() => this.toggle()
																		)}
																>
																	<FaFileImage />
																</Button>
															)}
														</td>
													</tr>
												))
											) : (
												<div>---- ไม่มีรายการห้องประชุม -----</div>
											)}
										</tbody>
									</Table>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
				<Modal isOpen={modal} toggle={this.toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={this.toggle}>
						สลิปการชำระค่าลงทะเบียน
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<img
										src={api_regis + id_registration + '.png'}
										style={{ width: '435px', height: 'auto' }}
									/>
								</Col>
							</Row>
						</Container>
					</ModalBody>
					{check === true && (
						<ModalFooter>
							<Button onClick={this.status_meeting} color="primary">
								อนุมัติ
							</Button>
						</ModalFooter>
					)}
				</Modal>
			</div>
		);
	}
	status_meeting = async () => {
		Swal.fire({
			title: 'Are you sure?',
			text: 'คุณต้องการอนุมัติใช้หรือไม่!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then(async (result) => {
			if (result.value) {
				try {
					let { id_meeting } = this.state;
					let res = await post('/status_meeting', {
						id_meeting,
						status_meeting: 'on'
					});
					if (res.success) {
						Swal.fire('สำเร็จ', res.message, 'success').then(() => window.location.reload());
					} else {
						Swal.fire('ผิดพลาด', 'ผิดพลาด', 'error');
					}
				} catch (error) {
					Swal.fire('ผิดพลาด', error, 'error');
					console.log(error);
				}
			}
		});
	};
}
